const axios = require('axios');
const https = require('https')
// Number of concurrent requests to send
const concurrency = 10;

// Total number of requests to send
const totalRequests = 1000;

// Endpoint URL
const url = 'https://localhost:7022/schemes/Scheme1/tables/Users/records';

// Function to send GET requests
async function sendRequest() {
  try {
    const startTime = Date.now();
    const response = await axios.get(url, {
      // Ignore SSL certificate verification (only for testing)
      httpsAgent: new https.Agent({ rejectUnauthorized: false }),
    });
    const endTime = Date.now();
    const duration = endTime - startTime;
    
    console.log(`Request sent successfully - Status: ${response.status} - Duration: ${duration}ms`);

    return {
      status: response.status,
      duration: duration
    };
  } catch (error) {
    console.error('Error sending request:', error.message);
    return {
      status: 'Error',
      duration: 0
    };
  }
}

// Function to run the load test
async function runLoadTest() {
  const requestPromises = [];

  // Create an array of request promises
  for (let i = 0; i < totalRequests; i++) {
    requestPromises.push(sendRequest());
  }

  const startTime = Date.now();

  // Wait for all requests to complete
  const responses = await Promise.all(requestPromises);
  
  const endTime = Date.now();
  const duration = endTime - startTime;

  const successfulResponses = responses.filter(response => response.status === 200);

  const totalSuccessfulRequests = successfulResponses.length;
  const throughput = totalSuccessfulRequests / (duration / 1000); // Requests per second

  console.log('Load test completed');
  console.log('Duration:', duration, 'ms');
  console.log('Total Requests:', totalRequests);
  console.log('Successful Requests:', totalSuccessfulRequests);
  console.log('Throughput:', throughput.toFixed(2), 'RPS');
}

// Run the load test
runLoadTest();
